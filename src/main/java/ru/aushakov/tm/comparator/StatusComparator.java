package ru.aushakov.tm.comparator;

import ru.aushakov.tm.api.entity.IHasStatus;
import ru.aushakov.tm.enumerated.Status;

import java.util.Comparator;

public final class StatusComparator implements Comparator<IHasStatus> {

    private static final StatusComparator INSTANCE = new StatusComparator();

    private StatusComparator() {
    }

    public static StatusComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasStatus o1, final IHasStatus o2) {
        if (o1 == null || o2 == null) return ((o1 == null) ? ((o2 == null) ? 0 : -1) : 1);
        final Status status1 = o1.getStatus();
        final Status status2 = o2.getStatus();
        if (status1 == null || status2 == null) return ((status1 == null) ? ((status2 == null) ? 0 : -1) : 1);
        return status1.compareTo(status2);
    }

}
