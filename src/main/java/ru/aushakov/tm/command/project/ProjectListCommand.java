package ru.aushakov.tm.command.project;

import ru.aushakov.tm.api.service.IProjectService;
import ru.aushakov.tm.command.AbstractProjectCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.SortType;
import ru.aushakov.tm.model.Project;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_LIST;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        List<Project> projects;
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + " OR PRESS ENTER:");
        final SortType sortType = SortType.toSortType(TerminalUtil.nextLine());
        final IProjectService projectService = serviceLocator.getProjectService();
        if (sortType == null) projects = projectService.findAll();
        else projects = projectService.findAll(sortType.getComparator());
        if (projects == null || projects.size() < 1) {
            System.out.println("[NOTHING FOUND]");
            return;
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
