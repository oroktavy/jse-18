package ru.aushakov.tm.command.system;

import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.general.FailedToSufferException;
import ru.aushakov.tm.util.TerminalUtil;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_LOGOUT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Who's using this anyway?";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        System.out.println("DO YOU WANT TO SUFFER? [Y/N]");
        final String choice = TerminalUtil.nextLine();
        if ("Nope".equals(choice)) throw new FailedToSufferException();
        System.out.println("SUFFER");
        serviceLocator.getAuthService().logout();
    }

}
