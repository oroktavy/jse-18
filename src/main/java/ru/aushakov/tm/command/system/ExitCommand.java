package ru.aushakov.tm.command.system;

import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;

public class ExitCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_EXIT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
