package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.IUserRepository;
import ru.aushakov.tm.model.User;
import ru.aushakov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findOneById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findOneByLogin(final String login) {
        for (final User user : users) {
            if (login.equalsIgnoreCase(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeOneById(final String id) {
        final User user = findOneById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    @Override
    public User removeOneByLogin(final String login) {
        final User user = findOneByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    @Override
    public User setPassword(
            final String login,
            final String newPassword
    ) {
        final User user = findOneByLogin(login);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(newPassword));
        return user;
    }

    @Override
    public User updateOneById(
            final String id,
            final String lastName,
            final String firstName,
            final String middleName
    ) {
        final User user = findOneById(id);
        if (user == null) return null;
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User updateOneByLogin(
            final String login,
            final String lastName,
            final String firstName,
            final String middleName
    ) {
        final User user = findOneByLogin(login);
        if (user == null) return null;
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        return user;
    }

}
