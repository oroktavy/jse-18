package ru.aushakov.tm.exception.empty;

public class EmptyUserException extends RuntimeException {

    public EmptyUserException() {
        super("Can not operate on empty user!");
    }

}
