package ru.aushakov.tm.exception.general;

public class WrongCredentialsException extends RuntimeException{

    public WrongCredentialsException() {
        super("Wrong credentials, login failed!");
    }

}
