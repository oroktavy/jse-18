package ru.aushakov.tm.exception.general;

public class FailedToSufferException extends RuntimeException {

    public FailedToSufferException() {
        super("You failed to suffer!");
    }

}
