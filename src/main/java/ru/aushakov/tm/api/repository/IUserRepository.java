package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.model.User;

public interface IUserRepository {

    User add(User user);

    User findOneById(String id);

    User findOneByLogin(String login);

    User removeOneById(String id);

    User removeOneByLogin(String login);

    User setPassword(String login, String newPassword);

    User updateOneById(String id, String lastName, String firstName, String middleName);

    User updateOneByLogin(String login, String lastName, String firstName, String middleName);

}
